#include <stdio.h>
#include <stdlib.h>

#include "list.h"
#include "trans_list.h"


typedef struct{
	int id ;
	size_t addr ;

	struct list_head list ;
	struct trans_list_head trans_list ;
} Node ;


#define ADDR_BASE	0xFF0000

int main(void){
	LIST_HEAD(head) ;
	TRANS_LIST_HEAD(trans_head, trans_log) ;

	int len[] = {4, 3, 5, 4} ;
	int i = 0 ;
	for( int j=0 ; j<sizeof(len)/sizeof(len[0]) ; j++ ){
		for( int k=0 ; k<len[j] ; k++ ){
			Node *node = (Node*) malloc(sizeof(Node)) ;
			node->id = i++ ;
			node->addr = (size_t)((long double*)ADDR_BASE+k) ;

			list_add_tail( &node->list, &head ) ;
			trans_list_add_tail( &node->trans_list, &trans_head, &trans_log, node->addr ) ;
		}
	}

	puts("By Input Order:") ;
	for( struct list_head *lh = head.next ; lh!=&head ; lh=lh->next ){
		Node *node = list_entry(lh, Node, list) ;
		printf( "%4d: 0x%llX\n", node->id, (unsigned long long)node->addr ) ;
	}

	puts("By Address:") ;
	for( struct trans_list_head *lh = trans_head.next ; lh!=&trans_head ; lh=lh->next ){
		Node *node = list_entry(lh, Node, trans_list) ;
		printf( "%4d: 0x%llX\n", node->id, (unsigned long long)node->addr ) ;
	}

	return 0 ;
}

